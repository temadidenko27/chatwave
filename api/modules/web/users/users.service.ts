import { getLogger } from '../../../common/logging';
import { usersRepository } from './users.repository';
import { IUser } from './users.types';
import { generateToken } from '../../common/utils/token.utils';
import { hashPassword, comparePassword } from '../../common/utils/password.utils';

const createUser = async (data: Partial<IUser>) => {
  const { user_name, email, password } = data;
  const log = getLogger();

  const user: Array<IUser> = await usersRepository.getUserByEmailName(email);

  if (user.length && user[0].email === email) {
    return { status: 400, message: `User with email: ${email} already exists` };
  }

  const hash_password: string = hashPassword(password);
  const newUser: IUser = {
    ...data,
    password: hash_password,
  };

  const createdUser: Array<IUser> = await usersRepository.createUser(newUser);
  log.info(`User with email: ${email} was created`);

  const token: string = generateToken(createdUser[0].id);

  return { status: 200, token, ...createdUser[0] };
};

const loginUser = async (data: Partial<IUser>) => {
  const { email, password } = data;

  const user: Array<IUser> = await usersRepository.getUserByEmailName(email);

  if (!user.length) {
    return { status: 400, message: `The credentials ${email} with EMAIL is incorrect` };
  }

  if (!comparePassword(user[0].password, password)) {
    return { status: 400, message: `The credentials ${email} with PASSWORD is incorrect` };
  }

  const token: string = generateToken(user[0].id);

  return { status: 200, token, ...user[0] };
};

export const usersService = {
  createUser,
  loginUser,
};
