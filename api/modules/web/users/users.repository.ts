import { IUser } from './users.types';
import { db } from '../../../common/db/knex';

const getUserByEmailName = async (email: string, user_name?: string): Promise<Array<IUser>> => {
  return db
    .select('*')
    .from('users')
    .modify((q) => {
      q.where({ email });
      if (user_name) q.where({ user_name });
    })
    .returning('*');
};

const createUser = async (data: IUser): Promise<Array<IUser>> => {
  return db('users').insert(data).returning('*');
};

const getUserById = async (id: number): Promise<Array<IUser>> => {
  return db.select('*').from('users').where({ id }).returning('*');
};

export const usersRepository = {
  getUserByEmailName,
  createUser,
  getUserById,
};
