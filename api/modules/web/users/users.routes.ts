import { Router } from 'express';
import { UsersController } from './users.controller';
import { validateSchema } from '../../../common/middlewares/validate';
import { userSchemaRegistration, userSchemaLogin } from './middlewares/validation/user.schema';

export const createUsersRouter = () => {
  const router = Router();
  router.post('/registration', validateSchema(userSchemaRegistration), UsersController.createUser);
  router.post('/login', validateSchema(userSchemaLogin), UsersController.login);

  return router;
};
