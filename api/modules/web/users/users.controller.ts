import { ExpressRequest, ExpressResponse } from '../../../common/types';
import { usersService } from './users.service';
import fs from 'fs';
import { IUser } from './users.types';

const createUser = async (req: ExpressRequest, res: ExpressResponse) => {
  const result = await usersService.createUser(req.query);

  return res.status(result.status).json(result);
};

const login = async (req: ExpressRequest, res: ExpressResponse) => {
  const result = await usersService.loginUser(req.query);

  return res.status(result.status).json(result);
};

export const UsersController = {
  createUser,
  login,
};
