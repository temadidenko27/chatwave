import Joi from 'joi';

export const userSchemaRegistration = Joi.object({
  user_name: Joi.string().required().min(2).trim(),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required()
    .trim(),
  password: Joi.string().min(8).pattern(new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}/)).required().trim(),
});

export const userSchemaLogin = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required()
    .trim(),
  password: Joi.string().min(8).pattern(new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}/)).required().trim(),
});
