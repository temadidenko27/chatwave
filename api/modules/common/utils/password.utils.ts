import bcrypt from 'bcrypt';


export const hashPassword = (password: string): string => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8))
}

export const comparePassword = (hashPassword: string, password: string): boolean => {
  return bcrypt.compareSync(password, hashPassword);
}